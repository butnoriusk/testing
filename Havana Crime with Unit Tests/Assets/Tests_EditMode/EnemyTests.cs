﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class EnemyTests
    {
        [Test]
        public void EnemyTakeDamageTest()
        {
            var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(0, 0, 0), Quaternion.identity);
            enemy.GetComponent<EnemyStats>().setStats(2);
            var defaultHp = enemy.GetComponent<EnemyStats>().HP;
            enemy.GetComponent<EnemyStats>().Damage(10);
            var currentHp = enemy.GetComponent<EnemyStats>().HP;

            MonoBehaviour.DestroyImmediate(enemy);
            Assert.Greater(defaultHp, currentHp);
        }     

        [Test]
        public void EnemySpawnerTest()
        {
            Transform enemy = Resources.Load("Tests/enemy", typeof(Transform)) as Transform;
            var spawner = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Spawner"), new Vector3(14, 3, 0), Quaternion.identity);
            spawner.GetComponent<WaveSpawner>().SpawnEnemy(enemy);

            MonoBehaviour.DestroyImmediate(spawner);
            if (GameObject.Find("enemy(Clone)") != null)
                Assert.That(true);
            else
                Assert.That(false);
        }

        //[Test]
        public void GetExpAfterKillEnemyTest()
        {
            var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
            player.name = "Player";
            var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(0, 0, 0), Quaternion.identity);
            enemy.GetComponent<EnemyStats>().setStats(2);
            var defaultExp = player.GetComponent<PlayerStats>().currentExp;
            enemy.GetComponent<EnemyStats>().AddEXP(10);
            var currentExp = player.GetComponent<PlayerStats>().currentExp;

            MonoBehaviour.DestroyImmediate(enemy);
            MonoBehaviour.DestroyImmediate(player);
            Assert.Greater(currentExp, defaultExp);
        }


        [Test]
        public void DestroyEnemyObjectAfterDieTest()
        {
            var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(0, 0, 0), Quaternion.identity);         
            enemy.name = "enemy";
            enemy.GetComponent<EnemyStats>().Die();

            MonoBehaviour.DestroyImmediate(enemy);
            Assert.That(GameObject.Find("enemy") == null);          
        }

        [Test]
        public void BloodAppearsAfterEnemyDeathTest()
        {
            var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(0, 0, 0), Quaternion.identity);
            enemy.GetComponent<EnemyStats>().Die();

            MonoBehaviour.DestroyImmediate(enemy);
            Assert.That(GameObject.Find("bloodDead(Clone)") != null);
        }

    }
}

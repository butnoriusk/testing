﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests
{
    public class MenuTests
    {
        [Test]
        public void PauseGameTest()
        {
            var menu = new GameObject().AddComponent<CharacteristicsMenu>();
            menu.charMenu = new GameObject();
           
            menu.Pause();
            var isPaused = menu.charMenu.active;
            menu.Resume();
            
            MonoBehaviour.DestroyImmediate(menu.charMenu);
            MonoBehaviour.DestroyImmediate(menu);
            Assert.AreEqual(isPaused, true);
        }

        [Test]
        public void ResumeGameTest()
        {
            var menu = new GameObject().AddComponent<CharacteristicsMenu>();
            menu.charMenu = new GameObject();

            menu.Pause();
            var isPaused = menu.charMenu.active;
            menu.Resume();
            var isPausedAfterResume = menu.charMenu.active;
            menu.Resume();

            MonoBehaviour.DestroyImmediate(menu.charMenu);
            MonoBehaviour.DestroyImmediate(menu);
            if (isPaused && !isPausedAfterResume)
                Assert.That(true);
            else
                Assert.That(false);
        }
    }
}
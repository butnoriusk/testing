﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class WeaponTests
    {

        [Test]
        public void AddRiffleAmmoTest()
        {
            var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
            player.name = "Player";

            player.AddComponent<WeaponScript>();
            player.GetComponent<WeaponScript>().bulletMax = 50;
            player.GetComponent<WeaponScript>().bulletCurrent = 1;
            var ammo = player.GetComponent<WeaponScript>().bulletCurrent;
            player.GetComponent<WeaponScript>().AddRifleAmmo(15);
            var ammoAfterAdd = player.GetComponent<WeaponScript>().bulletCurrent;

            MonoBehaviour.DestroyImmediate(player);
            Assert.That(ammo == 1 && ammoAfterAdd == 16);
        }

        [Test]
        public void AddShotgunAmmoTest()
        {          
            var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
            player.name = "Player";

            player.AddComponent<WeaponScriptShotgun>();
            player.GetComponent<WeaponScriptShotgun>().bulletMax = 50;
            player.GetComponent<WeaponScriptShotgun>().bulletCurrent = 1;
            var ammo = player.GetComponent<WeaponScriptShotgun>().bulletCurrent;
            player.GetComponent<WeaponScriptShotgun>().AddShotgunAmmo(15);
            var ammoAfterAdd = player.GetComponent<WeaponScriptShotgun>().bulletCurrent;

            MonoBehaviour.DestroyImmediate(player);
            Assert.That(ammo == 1 && ammoAfterAdd == 16);
        }

        [Test]
        public void AddMoreThanMaxShotgunAmmoTest()
        {
            var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
            player.name = "Player";

            player.AddComponent<WeaponScriptShotgun>();
            player.GetComponent<WeaponScriptShotgun>().bulletMax = 50;
            player.GetComponent<WeaponScriptShotgun>().bulletCurrent = 1;
            var ammo = player.GetComponent<WeaponScriptShotgun>().bulletCurrent;
            player.GetComponent<WeaponScriptShotgun>().AddShotgunAmmo(100);
            var ammoAfterAdd = player.GetComponent<WeaponScriptShotgun>().bulletCurrent;

            MonoBehaviour.DestroyImmediate(player);
            Assert.That(ammo == 1 && ammoAfterAdd == 50);
        }

        [Test]
        public void AddMoreThanMaxRifleAmmoTest()
        {
            var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
            player.name = "Player";

            player.AddComponent<WeaponScript>();
            player.GetComponent<WeaponScript>().bulletMax = 50;
            player.GetComponent<WeaponScript>().bulletCurrent = 1;
            var ammo = player.GetComponent<WeaponScript>().bulletCurrent;
            player.GetComponent<WeaponScript>().AddRifleAmmo(100);
            var ammoAfterAdd = player.GetComponent<WeaponScript>().bulletCurrent;

            MonoBehaviour.DestroyImmediate(player);
            Assert.That(ammo == 1 && ammoAfterAdd == 50);
        }
    }
}

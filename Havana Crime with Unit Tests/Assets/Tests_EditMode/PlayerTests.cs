﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class PlayerTests
    {
        [Test]
        public void UpgradeHitpointsTest()
        {
            var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
            player.GetComponent<PlayerStats>().setStats();
            var defaultHp = player.GetComponent<PlayerStats>().currentHP;
            player.GetComponent<PlayerStats>().SetCharacteristic("HP");
            var upgradedHp = player.GetComponent<PlayerStats>().currentHP;

            MonoBehaviour.DestroyImmediate(player);
            Assert.Greater(upgradedHp, defaultHp);
        }

        [Test]
        public void UpgradeDamageTest()
        {
            var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
            player.GetComponent<PlayerStats>().setStats();
            var defaultDMG = player.GetComponent<PlayerStats>().currentDMG;
            player.GetComponent<PlayerStats>().SetCharacteristic("DMG");
            var upgradedDMG = player.GetComponent<PlayerStats>().currentDMG;

            MonoBehaviour.DestroyImmediate(player);
            Assert.Greater(upgradedDMG, defaultDMG);
        }

        [Test]
        public void UpgradeDefenceTest()
        {
            var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
            player.GetComponent<PlayerStats>().setStats();
            var defaultDEF = player.GetComponent<PlayerStats>().currentDEF;
            player.GetComponent<PlayerStats>().SetCharacteristic("DEF");
            var upgradedDEF = player.GetComponent<PlayerStats>().currentDEF;

            MonoBehaviour.DestroyImmediate(player);
            Assert.Greater(upgradedDEF, defaultDEF);
        }

        [Test]
        public void PlayerLevelUpTest()
        {
            var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
            player.GetComponent<PlayerStats>().setStats();
            var levelBefore = player.GetComponent<PlayerStats>().currentLevel;
            player.GetComponent<PlayerStats>().currentExp = 100;

            player.GetComponent<PlayerStats>().Updates();
            var currentexp = player.GetComponent<PlayerStats>().currentExp;
            var levelAfter = player.GetComponent<PlayerStats>().currentLevel;

            MonoBehaviour.DestroyImmediate(player);
            Assert.Greater(levelAfter, levelBefore);
        }
    }
}
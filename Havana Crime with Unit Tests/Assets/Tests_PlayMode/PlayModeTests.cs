﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

public class PlayModeTests
{
    GameObject map1;
    GameObject map2;
    GameObject camera;
    GameObject astar;  

    [UnityTest]
    public IEnumerator SpeedBoostTest()
    {
        InstantiateBasics();
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
        yield return new WaitForSeconds(2);
        float defaultSpeed = player.GetComponent<PlayerMobility>().speed;
        var shoes = MonoBehaviour.Instantiate(Resources.Load<GameObject>("shoes"), new Vector3(18, 3, 0), Quaternion.identity);

        yield return new WaitForSeconds(0.5f);
        float newSpeed = player.GetComponent<PlayerMobility>().speed;
        yield return new WaitForSeconds(1);

        DeleteObjects(player);
        Assert.Greater(newSpeed, defaultSpeed);
    }

    [UnityTest]
    public IEnumerator V1_GameOverScreenTest()
    {
        InstantiateBasics();
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
        var scene1 = SceneManager.GetActiveScene();
        yield return new WaitForSeconds(1);
        player.GetComponent<HealthBar>().hitpoints = 0;           
        yield return new WaitForSeconds(1);
        var scene2 = SceneManager.GetActiveScene();
        yield return new WaitForSeconds(1);
        DeleteObjects(player);
        yield return new WaitForSeconds(1);
        Assert.AreNotEqual(scene1, scene2);
    }

    [UnityTest]
    public IEnumerator FloatingUpDamageTextTest()
    {
        InstantiateBasics();
        var text = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Textas"), new Vector3(18, 3, 0), Quaternion.identity);
        var positionBefore = text.transform.position.y;
        text.GetComponent<DestroyOverTime>().timeToDestroy = 3;
        yield return new WaitForSeconds(2);
        var positionAfter = text.transform.position.y;

        DeleteObjects(text);
        Assert.Greater(positionAfter,positionBefore);
    }

    [UnityTest]
    public IEnumerator DmgTextSpeedChangingTest()
    {
        InstantiateBasics();
        var text = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Textas"), new Vector3(14, -1, 0), Quaternion.identity);
        text.GetComponent<DestroyOverTime>().timeToDestroy = 5;

        text.GetComponent<FloatingNumbers>().moveSpeed = 1;
        var positionBefore = text.transform.position.y;
        yield return new WaitForSeconds(2);
        var positionAfter = text.transform.position.y;
        var diff1 = positionAfter - positionBefore;

        text.GetComponent<FloatingNumbers>().moveSpeed = 3;
        positionBefore = text.transform.position.y;
        yield return new WaitForSeconds(2);
        positionAfter = text.transform.position.y;
        var diff2 = positionAfter - positionBefore;

        DeleteObjects(text);
        Assert.Greater(diff2, diff1*2);
    }

    [UnityTest]
    public IEnumerator DestroyObjectOverTimeTest()
    {
        InstantiateBasics();
        var Player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(16, 3, 0), Quaternion.identity);
        Player.name = "Player";
        Player.AddComponent<DestroyOverTime>();
        Player.GetComponent<DestroyOverTime>().timeToDestroy = 1;

        yield return new WaitForSeconds(1.1f);
        Player = GameObject.Find("Player");
        yield return new WaitForSeconds(1.5f);

        DeleteObjects(Player);
        Assert.That(Player == null );     
    }

    public void InstantiateBasics()
    {
        map1 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("map2_img"));
        map2 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("MapObjects"));
        camera = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Main Camera"));
        astar = MonoBehaviour.Instantiate(Resources.Load<GameObject>("A_"));
    }

    public void DeleteObjects(List<GameObject> objects)
    {
        foreach (var ob in objects)
        {
            MonoBehaviour.DestroyImmediate(ob);
        }
        MonoBehaviour.DestroyImmediate(map1);
        MonoBehaviour.DestroyImmediate(map2);
        MonoBehaviour.DestroyImmediate(camera);
        MonoBehaviour.DestroyImmediate(astar);
    }

    public void DeleteObjects(GameObject ob)
    {
        MonoBehaviour.DestroyImmediate(ob);
        MonoBehaviour.DestroyImmediate(map1);
        MonoBehaviour.DestroyImmediate(map2);
        MonoBehaviour.DestroyImmediate(camera);
        MonoBehaviour.DestroyImmediate(astar);
    }
}

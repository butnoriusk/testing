﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

public class EnemyTests
{
    GameObject map1;
    GameObject map2;
    GameObject camera;
    GameObject astar;

    [UnityTest]
    public IEnumerator EnemyFollowingPlayerTest()
    {
        InstantiateBasics();
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
        var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(0, 3, 0), Quaternion.identity);
        List<GameObject> objects = new List<GameObject>() { player, enemy };

        float distance1 = player.transform.position.x - enemy.transform.position.x; //pradinis atstumas tarp zaidejo ir zombio
        yield return new WaitForSeconds(2);
        float distance2 = player.transform.position.x - enemy.transform.position.x;// atstumas kai zombis seka zaideja 1s

        DeleteObjects(objects);
        Assert.Greater(distance1, distance2);
    }

    [UnityTest]
    public IEnumerator MolotovDamageTest()
    {
        InstantiateBasics();
        var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(18, 3, 0), Quaternion.identity);
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(-40, 3, 0), Quaternion.identity);
        var molotov = MonoBehaviour.Instantiate(Resources.Load<GameObject>("molotov"), new Vector3(9, 3, 0), Quaternion.identity);
        enemy.GetComponent<EnemyStats>().setStats(2);
        List<GameObject> objects = new List<GameObject>() { player, enemy };

        yield return new WaitForSeconds(0.5f);
        float defaultEnemyHp = enemy.GetComponent<EnemyStats>().HP;
        yield return new WaitForSeconds(3);
        float currentenemyHP = enemy.GetComponent<EnemyStats>().HP;

        DeleteObjects(objects);
        MonoBehaviour.DestroyImmediate(GameObject.Find("blood1(Clone)"));
        Assert.Greater(currentenemyHP, defaultEnemyHp );
    } 

    [UnityTest]
    public IEnumerator EnemyDoDamageTest()
    {
        InstantiateBasics();
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
        var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(15, 0, 0), Quaternion.identity);
        enemy.GetComponent<EnemyStats>().setStats(2);
        player.GetComponent<PlayerStats>().setStats();
        var defaultHp = player.GetComponent<HealthBar>().hitpoints;
        yield return new WaitForSeconds(4);
        var currentHp = player.GetComponent<HealthBar>().hitpoints;

        DeleteObjects(player);
        DeleteObjects(enemy);
        Assert.Greater(defaultHp, currentHp);
    }

    [UnityTest]
    public IEnumerator GetExpAfterEnemyDeathTest()
    {
        InstantiateBasics();
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(18, 3, 0), Quaternion.identity);
        player.name = "Player";
        var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(0, 0, 0), Quaternion.identity);
        enemy.GetComponent<EnemyStats>().setStats(2);
        yield return new WaitForSeconds(1.5f);
        var defaultExp = player.GetComponent<PlayerStats>().currentExp;
        enemy.GetComponent<EnemyStats>().AddEXP(10);
        enemy.GetComponent<EnemyStats>().HP = 0;
        yield return new WaitForSeconds(2);
        var currentExp = player.GetComponent<PlayerStats>().currentExp;

        MonoBehaviour.DestroyImmediate(enemy);
        MonoBehaviour.DestroyImmediate(GameObject.Find("bloodDead(Clone)"));
        DeleteObjects(player);
        Assert.Greater(currentExp, defaultExp);
    }

    [UnityTest]
    public IEnumerator BulletDoDamageEnemyTest()
    {
        InstantiateBasics();
        var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(18, 3, 0), Quaternion.identity);
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(-20, 3, 0), Quaternion.identity);
        var bullet = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Bullet"), new Vector3(14, 3, 0), Quaternion.identity);
        enemy.GetComponent<EnemyStats>().setStats(2);
        List<GameObject> objects = new List<GameObject>() { bullet, enemy, player };

        float defaultEnemyHp = enemy.GetComponent<EnemyStats>().HP;
        yield return new WaitForSeconds(1f);
        float currentenemyHP = enemy.GetComponent<EnemyStats>().HP;
        yield return new WaitForSeconds(2);
        DeleteObjects(objects);
        Assert.Greater(defaultEnemyHp, currentenemyHP);
    }

    [UnityTest]
    public IEnumerator SpawnBloodAfterBulletAndEnemyCollisionTest()
    {
        InstantiateBasics();
        var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(18, 3, 0), Quaternion.identity);
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(-20, 3, 0), Quaternion.identity);
        var bullet = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Bullet"), new Vector3(14, 3, 0), Quaternion.identity);
        enemy.GetComponent<EnemyStats>().setStats(2);
        List<GameObject> objects = new List<GameObject>() { bullet, enemy, player };

        yield return new WaitForSeconds(1f);
        var isBloodAppears = false;
        if (GameObject.Find("blood1(Clone)") != null)
            isBloodAppears = true;
        yield return new WaitForSeconds(2);

        DeleteObjects(objects);
        Assert.That(isBloodAppears);
    }

    [UnityTest]
    public IEnumerator ShotgunBulletDoDamageEnemyTest()
    {
        InstantiateBasics();
        var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(18, 3, 0), Quaternion.identity);
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(-20, 3, 0), Quaternion.identity);
        var bullet = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Bullet_Shotgun"), new Vector3(14, 3, 0), Quaternion.identity);
        enemy.GetComponent<EnemyStats>().setStats(2);
        List<GameObject> objects = new List<GameObject>() { bullet, enemy, player };

        float defaultEnemyHp = enemy.GetComponent<EnemyStats>().HP;
        yield return new WaitForSeconds(1f);
        float currentenemyHP = enemy.GetComponent<EnemyStats>().HP;
        yield return new WaitForSeconds(2);
        DeleteObjects(objects);
        Assert.Greater(defaultEnemyHp, currentenemyHP);
    }

    [UnityTest]
    public IEnumerator BulletDissapearsAfterCollision()
    {
        InstantiateBasics();
        var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(18, 3, 0), Quaternion.identity);
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(-20, 3, 0), Quaternion.identity);
        var bullet = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Bullet"), new Vector3(14, 3, 0), Quaternion.identity);
        enemy.GetComponent<EnemyStats>().setStats(2);
        List<GameObject> objects = new List<GameObject>() { bullet, enemy, player };

        yield return new WaitForSeconds(1f);
        var isBulletDisappear = false;
        if (GameObject.Find("Bullet(Clone)") == null)
            isBulletDisappear = true;
        yield return new WaitForSeconds(1);

        DeleteObjects(objects);
        Assert.That(isBulletDisappear);
    }


    public void InstantiateBasics()
    {
        map1 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("map2_img"));
        map2 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("MapObjects"));
        camera = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Main Camera"));
        astar = MonoBehaviour.Instantiate(Resources.Load<GameObject>("A_"));
    }

    public void DeleteObjects(List<GameObject> objects)
    {
        foreach (var ob in objects)
        {
            MonoBehaviour.DestroyImmediate(ob);
        }
        MonoBehaviour.DestroyImmediate(map1);
        MonoBehaviour.DestroyImmediate(map2);
        MonoBehaviour.DestroyImmediate(camera);
        MonoBehaviour.DestroyImmediate(astar);
    }

    public void DeleteObjects(GameObject ob)
    {
        MonoBehaviour.DestroyImmediate(ob);
        MonoBehaviour.DestroyImmediate(map1);
        MonoBehaviour.DestroyImmediate(map2);
        MonoBehaviour.DestroyImmediate(camera);
        MonoBehaviour.DestroyImmediate(astar);
    }
}

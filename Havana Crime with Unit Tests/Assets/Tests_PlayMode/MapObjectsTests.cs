﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class MapObjectsTests
{
    GameObject map1;
    GameObject map2;
    GameObject camera;
    GameObject astar;

    [UnityTest]
    public IEnumerator LavaDamagingTest() // 20; -15  koordinates lavos
    {
        camera = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Main Camera"));
        map1 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("map2_img"));
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(20, -6, 0), Quaternion.identity);
        map1.transform.Find("lava").GetComponent<FloorTriggerZone>().player = player;
        var defaultExp = player.GetComponent<HealthBar>().hitpoints;
        yield return new WaitForSeconds(0.1f);
        player.transform.position = new Vector3(20, -12, 0);
        yield return new WaitForSeconds(0.1f);
        var currentExp = player.GetComponent<HealthBar>().hitpoints;

        DeleteObjects(player);
        Assert.Greater(defaultExp, currentExp);
    }

    [UnityTest]
    public IEnumerator WaterDecreaseSpeedTest()  // -16; -16  vandens koordinates, -16; 0 sausumos
    {
        camera = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Main Camera"));
        map1 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("map2_img"));
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(-16, 0, 0), Quaternion.identity);
        yield return new WaitForSeconds(1.1f);
        map1.transform.Find("water").GetComponent<FloorTriggerZone>().player = player;

        var defaultExp = player.GetComponent<PlayerMobility>().speed;
        yield return new WaitForSeconds(1.1f);
        player.transform.position = new Vector3(-16, -16, 0);
        yield return new WaitForSeconds(0.1f);
        var currentExp = player.GetComponent<PlayerMobility>().speed;

        DeleteObjects(player);
        Assert.Greater(defaultExp, currentExp);
    }

    [UnityTest]
    public IEnumerator HealingTest() // -26.2 13.8
    {
        camera = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Main Camera"));
        map1 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("map2_img"));
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(-26.2f, 13.8f, 0), Quaternion.identity);
        player.GetComponent<HealthBar>().hitpoints = 1;
        yield return new WaitForSeconds(0.1f);
        var currentHp = player.GetComponent<HealthBar>().hitpoints;

        DeleteObjects(player);
        Assert.Greater(currentHp, 1);
    }

    [UnityTest]
    public IEnumerator MolotovExplodesTest() 
    {
        InstantiateBasics();
        var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("FastEnemy"), new Vector3(15, 3, 0), Quaternion.identity);
        var player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"), new Vector3(-40, 3, 0), Quaternion.identity);
        var molotov = MonoBehaviour.Instantiate(Resources.Load<GameObject>("molotov"), new Vector3(12, 3, 0), Quaternion.identity);
        List<GameObject> objects = new List<GameObject>() { enemy, player };
        yield return new WaitForSeconds(1.5f);
        var explosion = GameObject.Find("molotov(Clone)"); 

        DeleteObjects(objects);
        MonoBehaviour.DestroyImmediate(GameObject.Find("blood1(Clone)"));
        Assert.That(explosion == null);
    }


    public void InstantiateBasics()
    {
        map1 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("map2_img"));
        map2 = MonoBehaviour.Instantiate(Resources.Load<GameObject>("MapObjects"));
        camera = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Main Camera"));
        astar = MonoBehaviour.Instantiate(Resources.Load<GameObject>("A_"));
    }

    public void DeleteObjects(List<GameObject> objects)
    {
        foreach (var ob in objects)
        {
            MonoBehaviour.DestroyImmediate(ob);
        }
        MonoBehaviour.DestroyImmediate(map1);
        MonoBehaviour.DestroyImmediate(map2);
        MonoBehaviour.DestroyImmediate(camera);
        MonoBehaviour.DestroyImmediate(astar);
    }

    public void DeleteObjects(GameObject ob)
    {
        MonoBehaviour.DestroyImmediate(ob);
        MonoBehaviour.DestroyImmediate(map1);
        MonoBehaviour.DestroyImmediate(map2);
        MonoBehaviour.DestroyImmediate(camera);
        MonoBehaviour.DestroyImmediate(astar);
    }
}